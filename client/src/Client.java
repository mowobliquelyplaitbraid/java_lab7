import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Client {
    private DatagramChannel udpChannel;
    private InetSocketAddress sAdress;
    private int port;
    private Scanner in;
    boolean e;
    Command com;

    /**
     * Constructor for client creation
     *
     * @param adr  - Adress
     * @param port - Port
     */
    private Client(String adr, int port) throws IOException {
        this.sAdress = new InetSocketAddress(InetAddress.getByName(adr), port);
        this.port = port;
        this.udpChannel = DatagramChannel.open();
        in = new Scanner(System.in);
    }

    /**
     * Method for contact with user on the client side. Invitation to enter, initial processing of the command
     */
    private void ex() {
        System.out.println("");
        System.out.println("Enter your command(s), please: (to navigate through the commands enter \"help\"). If your command consists information about an element, reading of this element ends with symbol \";\".");
        String command = "";
        String s = "";
        String login = "";
        String password = "";
        String email = "";
        boolean sFlag = false;
        Scanner scanner = new Scanner(System.in);


        System.out.println("For registration enter \"register\", then you should enter your username and email address. You'll reseive your password on it.");
        System.out.println("If you already have an account, enter \"login\" and then enter your username and password.");

        while (true) {
            s = scanner.nextLine().trim();
            if (s.equals("login")) {
                System.out.println("Enter login:");
                login = scanner.nextLine().trim();
                System.out.println("Enter password:");
                Console cnsl = System.console();
                if (cnsl != null) {
                    password = new String(cnsl.readPassword("Password:"));
                } else {
                    password = scanner.nextLine().trim();
                }

                Command com = new Command(login, password, "login", "log");
                sendRequest(com);
            } else if (s.equals("register")) {
                System.out.println("Enter login:");
                login = scanner.nextLine().trim();
                System.out.println("Enter email. You will receive your password on it.");
                email = scanner.nextLine().trim();
                if (email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")) {
                    Command com = new Command(login, "", email, "log");
                } else System.out.println("Wrong e-mail.");
            }
            if (com.getFromServer().equals("Done.")) {
                System.out.println("Welcome,  " + login);
                break;
            } else System.out.println("Enter login or register.");
        }

        while (!command.equals("exit")) {
            boolean flag = false;
            if (sFlag) System.exit(0);
            try {
                s = in.nextLine();
            } catch (NoSuchElementException e) {
                System.exit(0);
            }
            Plane plane = new Plane("SomePlane", new Human("Someone", EPlace.Base), EPlace.Base, 13);
            if (s.contains("{")) {
                int c = 0;
                while (!s.contains(";")) {
                    if (s.contains(("{"))) {
                        c++;
                    }
                    if (s.contains("}")) {
                        c--;
                        if (c == 0) {
                            break;
                        }
                    }
                    s += in.nextLine();
                }
                s = s.substring(0, s.length() - 1);
                command = s.substring(0, s.indexOf(' ')).trim();
                String json = s.substring(s.indexOf(' ') + 1);
                try {
                    if (json.contains("{}")) throw new Exception();
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    plane = gson.fromJson(json, Plane.class);
                    plane.setDate();
                } catch (Exception e) {
                    System.out.println("Wrong format of plane. Try again.");
                    flag = true;
                }
                com = new Command(plane, command);
            } else {
                command = s.trim();
                com = new Command(command);
                if (command.contains("import")) {
                    try {
                        Vector<Plane> planes = new Vector<>();
                        BufferedReader reader = new BufferedReader(new InputStreamReader((new FileInputStream(s.substring(s.indexOf('t') + 1)))));
                        Plane[] p = new Gson().fromJson(reader, Plane[].class);
                        for (Plane element : p) {
                            planes.add(element);
                        }
                        com.setPlanes(planes);
                    } catch (FileNotFoundException e) {
                        System.out.println("This file doesn't exist yet or you don't have permission for reading. Please, create one and try again.");
                    } catch (Exception e) {
                        System.out.println("Your input file is empty or has some wrong data in it.");
                    }
                }
            }
            if (flag) continue;
            if (command.equals("exit")) {
                System.out.println("Do you need to save your collection? y/n");
                if (new Scanner(System.in).nextLine().equals("n")) {
                    com.setE(false);
                } else {
                    com.setE(true);
                    System.out.println("The information will be saved.");
                    System.out.println("Do you want to rewrite the information from your output file? y/n");
                    command = new Scanner(System.in).nextLine();
                    com.setCommand(command);
                    sFlag = true;
                }
            }
            sendRequest(com);
        }

    }

    /**
     * Method sends a request with command in it to server
     *
     * @param com
     */
    private void sendRequest(Command com) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(8190);
        byteBuffer.clear();
        byte[] buffer;
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream output = new ObjectOutputStream(outputStream);
            output.flush();
            output.writeObject(com);
            buffer = outputStream.toByteArray();
            udpChannel.send(ByteBuffer.wrap(buffer), sAdress);

            udpChannel.receive(byteBuffer);
            buffer = byteBuffer.array();
            ByteArrayInputStream inputStream = new ByteArrayInputStream(buffer);
            ObjectInputStream input = new ObjectInputStream(inputStream);
            Command s = (Command) input.readObject();
            System.out.println(s.getFromServer());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("You need to write \"java -jar lab_7.jar <host><port>\"");
        }
        try {
//            Client client = new Client(args[0], Integer.parseInt(args[1]));
            Client client = new Client("localhost", 6666);
            client.ex();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
