import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class Sender {
    public static void send(String email, String password) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("There must be your email address", "And there must be your password");
                    }
                });

        try {
            MimeMessage mess;
            mess = new MimeMessage(session);
            mess.setFrom(new InternetAddress(email));
            mess.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            mess.setSubject("Password");
            mess.setText(password);
            Transport.send(mess);
            System.out.println("Password is on the way.");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}