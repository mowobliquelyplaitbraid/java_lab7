import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Vector;

public class DBHandler {
    private PreparedStatement preState;
    private Connection connect = null;
    Vector<String> users = new Vector<>();

    public DBHandler() {
        try {
            Class.forName("org.postgresql.Driver");
            connect = DriverManager.getConnection("jdbc:postgresql://localhost:6666/studs",
                    "postgres", "159ZyF753");
        } catch (SQLException e) {
            System.out.println("Have problems with connection to DB.");
        } catch (Exception e) {
            System.out.println("ClassNotFound. Ha-ha. Classic.");
        }
    }

    public Vector<Plane> load() {
        Vector<Plane> planes = new Vector<Plane>();
        try {
            preState = connect.prepareStatement("SELECT * FROM \"planes\";");
            ResultSet res = preState.executeQuery();
            while (res.next()) {
                System.out.println(res.getString(1) + res.getString(2) + res.getString(3));
                planes.add(new Plane(res.getString(2), new Human(res.getString(1), EPlace.valueOf(res.getString(3))), EPlace.valueOf(res.getString(3)), res.getInt(4),
                        LocalDateTime.parse(res.getString(5)), res.getString(6)));
            }
            return planes;
        } catch (SQLException s) {
            System.out.println("Error.");
        }
        return null;
    }

    public String addUser(String login, String password, String email, String sol) {
        try {
            Class.forName("org.postgresql.Driver");
            preState = connect.prepareStatement("SELECT * FROM auth WHERE email=?;");
            preState.setString(1, email);
            ResultSet res = preState.executeQuery();
            if (!res.next()) {
                preState.close();
                res.close();
                preState = connect.prepareStatement("INSERT INTO auth VALUES (?,?,?,?);");
                preState.setString(1, email);
                password = getMD5(password + sol);
                preState.setString(2, password);
                preState.setString(3, sol);
                preState.setString(4, login);
                preState.executeUpdate();
                preState.close();
                users.add(login);
                Sender sender = new Sender();
                sender.send(email, new PasswordGen().generate(login));
                return "Done.";
            } else return "We already have user with this email.";
        } catch (Exception e){
            return "Error with drivers's reg.";
        }
    }

    public String checkUser(String login, String password, String mail) {
        try {
            Class.forName("org.postgresql.Driver");
            preState = connect.prepareStatement("SELECT * FROM auth where username=?;");
            preState.setString(1, login);
            ResultSet res = preState.executeQuery();
            if (res.next()) {
                String pw = res.getString("pw");
                String sol = res.getString("sol");
                if (getMD5(password + sol).equals(pw)) {
                    res.close();
                    return "Done.";
                } else {
                    res.close();
                    return "No no no no no no.";
                }
            } else return "Login or password are incorrect.";
        } catch (Exception e) {
            return "Error with drivers's reg.";
        }
    }

//    public String showTable() {
//        Vector pl = new Vector();
//        try {
//            Class.forName("org.postgresql.Driver");
//            preState = connect.prepareStatement("SELECT * FROM planes;");
//            ResultSet res = preState.executeQuery();
//            while (res.next()) {
//                String name = res.getString("name");
//                int humanId = res.getInt("human");
//                EPlace place = EPlace.valueOf(res.getString("place"));
//                int number = res.getInt("number");
//                LocalDateTime date = res.getTimestamp("date").toLocalDateTime();
//                PreparedStatement preHuman = connect.prepareStatement("SELECT * FROM Human WHERE id=?;");
//                ResultSet resHuman = preHuman.executeQuery();
//                String Name = resHuman.getString("Name");
//                String Place = resHuman.getString("Place");
//                Human human = new Human(Name, EPlace.valueOf("Place"));
//                pl.add(new Plane(name, human, place, number, date, ));
//            }
//        } catch (Exception e){
//            System.out.println("Error with drivers's reg.");
//        }
//        return pl.toString();
//    }

    public void clearUserTable(String username){
        try {
            Class.forName("org.postgresql.Driver");
            preState = connect.prepareStatement("DELETE FROM planes where username=?;");
            preState.setString(1, username);
            preState.executeUpdate();
            preState.close();
        } catch (Exception e){
            System.out.println("Error with drivers's reg.");
        }
    }

    public void addPlane(Plane p, String username) {
        try {
            Class.forName("org.postgresql.Driver");
            preState = connect.prepareStatement("INSERT INTO planes VALUES (?,?,?,?,?,?);");
            preState.setString(1, p.getDeviceName());
            PreparedStatement preHuman = connect.prepareStatement("SELECT * FROM Human where Name=?;");
            preHuman.setString(1, p.getOwner());
            ResultSet res = preHuman.executeQuery();
            int humanId;
            if (res.next())
                humanId = res.getInt("id");
            else {
                preHuman = connect.prepareStatement("INSERT INTO Human VALUES (?,?);");
                preHuman.setString(1, p.getOwner());
                preHuman.setString(2, p.getOwnerPlace().toString());
            }
            res.close();
            preHuman.executeUpdate();
            preHuman.close();
            preState.setString(2, p.getOwner());
            preState.setString(3, p.getPlace().toString());
            preState.setInt(4, p.getNumber());
            preState.setString(5, p.getDate());
            preState.setString(6, username);
            preState.executeUpdate();
            preState.close();
        } catch (Exception e){
            System.out.println("Error with drivers's reg.");
        }
    }

    public void removePlane(Plane p, String login) {
        try {
            Class.forName("org.postgresql.Driver");
            preState = connect.prepareStatement("DELETE FROM planes where name=? and username=?;");
            preState.setString(1, p.getDeviceName());
            preState.setString(2, login);
            preState.executeUpdate();
            preState.close();
        } catch (Exception e) {
            System.out.println("Error with drivers's reg.");
        }
    }

    public String info() {
        String info = Plane.class.toString();
        try {
            Class.forName("org.postgresql.Driver");
            preState = connect.prepareStatement("SELECT COUNT(id) FROM planes;");
            ResultSet res = preState.executeQuery();
            info += "\ncollection contains: " + res.getInt(1);
            preState.close();
            preState = connect.prepareStatement("SELECT COUNT(LOGIN) FROM USERS;");
            ResultSet res1 = preState.executeQuery();
            info += "\ndate and time when collection was initialized " + res1.getInt(1);
            preState.close();
        } catch (Exception e) {
            System.out.println("Error with drivers's reg.");
        }
        return info;
    }

    public String getMD5(String password) {
        byte[] bytesOfPass = password.getBytes();
        byte[] passHashed = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            passHashed = md.digest(bytesOfPass);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Some problems with your hashing algorithm.");
        }
        return passHashed.toString();
    }
}
