public class ExistException extends NullPointerException {
    private String trouble  = " не указано.";
    ExistException(){
        trouble = "Имя человека не указано.";
    }

    ExistException(String s){
        super(s);
        trouble = s + trouble;
    }

    public String getExc() {
        return trouble;
    }
}
