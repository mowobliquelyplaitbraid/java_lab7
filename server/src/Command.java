import java.io.*;
import java.util.*;
import com.google.gson.Gson;

public class Command implements Serializable {
    private Plane plane = null;
    private Vector<Plane> planes = new Vector<>();
    private String command;
    private Date date;
    private boolean e;
    private String fromServer = "";
    private String login;
    private String password;
    private String email;
    DBHandler handler = new DBHandler();

    /**
     * Constructor for commands with elements
     *
     * @param p  - Element
     */
    Command(Plane p, String com) {
        plane = p;
        command = com;
        date = new Date();
    }

    Command(String l, String p, String e, String com) {
        login = l;
        password = p;
        email = e;
        command = com;
    }

    Command(String l, String p) {
        login = l;
        password = p;
    }

    /**
     * Constructor for commands without elements
     *
     * @param com - command
     */
    Command(String com) {
        command = com;
    }

    @Override
    public String toString() {
        return this.planes.toString();
    }

    public String getFromServer() {
        return fromServer;
    }

    public void setE(boolean E) {
        this.e = E;
    }

    public void setCommand(String s) {
        this.command = s;
    }

    public String getCommand() {
        return command;
    }

    public void setPlanes(Vector <Plane> p) {
        this.planes = p;
    }

    public Vector<Plane> getPlanes() {
        return planes;
    }

    public void sortPlanes() {
        Collections.sort(planes);
    }

    public String log() {
        if (email.equals("login")) {
            return handler.checkUser(login, password, email);
        } else {
            PasswordGen sol = new PasswordGen();
            return handler.addUser(login, password, email, sol.generate(login));
        }
    }

    /**
     * Command processing method
     */
    public void swCommand() {
        switch (this.command) {
            case "log":
                fromServer = this.log();
                break;
            case "add":
                planes = this.add();
                fromServer = "Done!";
                break;
            case "remove_greater":
                planes = this.remove_greater();
                fromServer = "Done!";
                break;
            case "show":
                fromServer = this.show();
                break;
            case "clear":
                planes = this.clear();
                fromServer = "Done!";
                break;
            case "info":
                fromServer = this.info(date);
                break;
            case "remove":
                planes = this.remove();
                fromServer = "Done!";
                break;
            case "add_if_min":
                planes = this.add_if_min();
                fromServer = "Done!";
                break;
            case "help":
                fromServer = this.help();
                break;
            case "exit":
                try {
                    System.out.println("Bye.");
                    handler.users.remove(this.login);
                    System.exit(0);
//                    fromServer = this.exit();
                } catch (Exception e) {
                    System.err.println("Congratulations! Program crash. But I've tried to save all the data in output file (of course, if it was possible), take a look.");
                }
                break;
//            case "y":
//                try {
//                    File file = new File("InitData.json");
//                    Gson gOut = new Gson();
//                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), "utf-8"));
//                    writer.write(gOut.toJson(planes));
//                    writer.close();
//                } catch (IOException e) {
//                    System.out.println("Something went wrong.");
//                }
//                fromServer = "Saved.";
//                break;
//            case "n":
//                try {
//                    File file = new File("InitData.json");
//                    Gson gOut = new Gson();
//                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "utf-8"));
//                    writer.append(gOut.toJson(planes));
//                    writer.close();
//                } catch (IOException e) {
//                    System.out.println("Something went wrong.");
//                }
//                fromServer = "Saved.";
//                break;
//            case "save":
//                fromServer = this.save();
//                break;
//            case "import":
//                fromServer = "You uploaded this collection from your file: " + this.planes;
//                break;
            default:
                fromServer = "Try once more.";
        }
    }

    /**
     * Method adds an element
     *
     * @return collection with element
     */
    public Vector<Plane> add() {
        if (plane != null) {
            planes.add(plane);
            handler.addPlane(plane, login);
        } else {
            System.out.println("Write this command only with an element.");
        }
        System.out.println("The plane has been added.");
        return planes;
    }

    /**
     * Method removes element(s)
     *
     * @return collection without elements which are greater than specified
     */
    public Vector<Plane> remove_greater() {
        if (planes.remove(planes.stream().max(Plane::compareTo).get())) {
            handler.removePlane(plane, login);
        }
        return planes;
    }

    /**
     * Method shows elements of collection
     */
    public String show() {
        return this.toString();
    }

    /**
     * Method clears collection
     *
     * @return collection without elements
     */
    public Vector<Plane> clear() {
        planes.clear();
        handler.clearUserTable(login);
        return planes;
    }

    /**
     * Method shows information about collection
     *
     * @param date
     */
    public String info(Date date) {
        //return planes.getClass() + "\ncollection contains: " + planes.stream().count() + "\ndate and time when collection was initialized " + date.toString();
        return handler.info();
    }

    /**
     * Method removes an element
     *
     * @return collection without an element
     */
    public Vector<Plane> remove() {
        planes.remove(planes.stream().filter(plane::equals).findFirst().get());
        return planes;
    }

    /**
     * Method adds an element if it is smaller than specified
     *
     * @return collection with element or without any changes
     */
    public Vector<Plane> add_if_min() {
        if (plane.compareTo(planes.stream().min(Plane::compareTo).get()) < 0) {
            if (planes.add(plane)) {
                handler.addPlane(plane, login);
                System.out.println("The plain has been added.");
            } else System.out.println("The plane isn't smaller than others.");
        }
        return planes;
    }

    /**
     * Method shows information about commands
     */
    public String help() {
        return "You can use this types of commands:\nadd {element} (adds an element in the collection)" +
                "\nremove_greater {element} (removes from the collection all elements exceeding specified)" +
                "\nshow (shows all elements from the collection)\nclear (removes all elements from the collection)" +
                "\ninfo (shows such information as type, number of elements and initialization date and time of the collection)" +
                "\nremove {element} (removes an elemend from the collection)" +
                "\nadd_if_min {element} (adds an element to the collection if its value is less than the smallest element's of this collection)" +
                "\nsave (saves all collection in an output file)" +
                "\nimport \"nameOfFile\" (imports a collection from your file)" +
                "\n\nthe field {element} must contain the following fields and their values in json format: \"name\", \"human\"(which contains such fields as" +
                "\"Name\" and \"Place\"), \"place\", \"number\" and \"date\" (in format MMM dd, yyyy, hh:mm:ss a).\nAlso you have only three types of places: Base, InFlight and McMerdo. Good luck(to me)!";
    }

    /**
     * Method closes the program
     *
     */
//    public String exit() {
////        if (e) {
//
//
////        }
//        return "Bye.";
//    }

    /**
     * Method saves the program data. Asks if user wants to add information in file or overwrite it
     *
     * @throws IOException
     */
//    public String save() {
//            return "Do you want to rewrite the information from your output file? y/n";
//    }
}