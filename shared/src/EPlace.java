//import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

enum EPlace implements Serializable {
//    @SerializedName("Base")
    Base("Base"),
  //  @SerializedName("InFlight")
    Flight("In Flight"),
    //@SerializedName("McMerdo")
    McMerdo("McMerdo");

    private final String Type;
    EPlace(String t) {
        Type = t;
    }

    @Override
    public String toString() {
        return Type;
    }
}
