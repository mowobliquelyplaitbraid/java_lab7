import java.io.Serializable;
import java.util.Objects;

public class Human implements IHuman, IMove, Serializable {
    private String Name;
    private EPlace Place;

    Human(String n, EPlace p) {
        Name = n;
        Place = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Name.equals(human.Name) &&
                Place == human.Place;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Name, Place);
    }

    @Override
    public String toString() {
        return Name + " находится на месте: " + Place;
    }

    public String getName() {
        return Name;
    }

    public EPlace getPlace() {
        return Place;
    }

    public void move(EPlace p) {
        Place = p;
    }

    public void getScared(Time time) {
        if (time.getTime().equals("В ту ночь")) {
            System.out.println(time.getTime() + " никто не мог заснуть крепким сном, все мы поминутно просыпались. Возбуждение было слишком велико.");
        }
        if (time.getTime().equals("18:00")) {
            System.out.println("К " + time.getTime() + " страх наш достиг апогея...");
        }
        if (time.getTime().equals("00:00.")) {
            System.out.println("Было довольно рискованно лететь всем в одном самолете над ледяным материком, не имея промежуточных баз, но никто не спасовал. Это был единственный выход.");
        }
        if (time.getTime().equals("5 января в 17:15")) {
            System.out.println("Но дурные предчувствия нас не покидали: что обнаружим мы у цели?");
        }
        if (time.getTime().equals("С этого времени")) {
            System.out.println(time.getTime() + " мы -- все десятеро, но особенно мы с Денфортом -- неотрывно следили за фантомами, таящимися в глубинах этого чудовищного искаженного мира, и ничто не заставит нас позабыть его.");
        }
    }

    public void reflection (Time time) {
        if (time.getTime().equals("5 января в 17:15")) {
            System.out.println("Мы были уверены, что с помощью указанных Лейком координат легко отыщем лагерь.");
        }
        if (time.getTime().equals("С этого времени")) {
            System.out.println("Мы не стали бы рассказывать, будь это возможно, о наших переживаниях всему человечеству. Газеты напечатали бюллетени, посланные нами с борта самолета, в которых сообщалось о нашем беспосадочном перелете; о встрече в верхних слоях атмосферы с предательскими порывами ветра; об увиденной с высоты шахте, которую Лейк пробурил три дня назад на полпути к горам, а также о загадочных снежных цилиндрах, замеченных ранее Амундсеном и Бэрдом,-- ветер гнал их по бескрайней ледяной равнине.");
            time.setTime("наступил момент");
        }
        if (time.getTime().equals("в 54-летнем возрасте")) {
            System.out.println("Именно тогда, " + time.getTime() + ", я навсегда утратил мир и покой, присущий человеку с нормальным рассудком и живущему в согласии с природой и ее законами.");
            time.setTime("С этого времени");
        }
        if (time.getTime().equals("наступил момент")) {
            System.out.println("Затем " + time.getTime() + ", когда мы не могли адекватно передавать охватившие нас чувства, а потом пришел и такой, когда мы стали строго контролировать свои слова, введя своего рода цензуру.");
            time.setTime("С этого времени");
        }
    }

    public void talkToOthers(Time time) {
        if (time.getTime().equals("00:00")) {
            System.out.println("Мы посовещались.");
        }
        if (time.getTime().equals("10:00")) {
            System.out.println("Однако нам удалось поговорить с \"Аркхемом\", и Дуглас сказал мне, что также не смог вызвать Лейка на связь. Об урагане он узнал от меня -- в районе залива Мак-Мердо было тихо, хотя в это верилось с трудом.");
            time.setTime("Весь день");
        }
        if (time.getTime().equals("Весь день")) {
            System.out.println(time.getTime() + " мы провели у приемника, прислушиваясь к малейшему шуму и потрескиванию в эфире, и время от времени тщетно пытались связаться с базой.");
            time.setTime("Около полудня");
        }
    }

    public void decision(Time time) {
        class Decision{
            private Time time;
            Decision(Time t){
                time = t;
            }
            void getDecision() {
                if (time.getTime().equals("18:00")) {
                    System.out.println("Я решил действовать.");
                }
                if (time.getTime().equals("момент наступил.")) {
                    System.out.println("Мы решили отправиться все вместе, захватив также сани и собак.");
                }
                if (time.getTime().equals("00:00.")) {
                    System.out.println("Мы решили лететь.");
                }
                if (time.getTime().equals("5 января в 17:15")) {
                    System.out.println("Каждый момент этого четырехчасового полета навсегда врезался в мою память: он изменил всю мою жизнь.");
                    time.setTime("в 54-летнем возрасте");
                }
            }
        }
        Decision d = new Decision(time);
        d.getDecision();
    }

    boolean getReadyToFlight(Time time) {
        if (time.getTime().equals("момент наступил.")) {
            System.out.println("Условия для полета были благоприятными.");
            return true;
        }
        if (time.getTime().equals("00:00.")) {
            System.out.println("Мы загрузили часть необходимого в самолет.");
            time.setTime("02:00");
            System.out.println("Около " + time.getTime() + " легли отдохнуть, но уже спустя четыре часа закончили паковать и укладывать вещи.");
            time.setTime("5 января в 17:15");
            return true;
        }
        return time.getTime().equals("19:30");
    }

    class Connection {
        Radio radio = new Radio(){
            void connect(String Name, Time time) {
                try {
                    if (getName().equals(Name)) throw new NameException(getName() + " не может связаться по радио с самим собой.");
                    if (Name.equals("Лейк")) {
                        if (time.getTime().equals("10:00")) {
                            System.out.println("В " + time.getTime() + " Мактай был уже на ногах и попытался связаться по рации с Лейком, но помешали атмосферные условия.");
                        }
                        if (time.getTime().equals("момент наступил.")) {
                            System.out.println("Готовясь к полету, я не прекращал попыток связаться с Лейком, но безуспешно.");
                        }
                        if (time.getTime().equals("После трех")) {
                            System.out.println("Мы с удвоенной энергией стали искать Лейка в эфире. Зная, что у него в распоряжении четыре радиофицированных самолета, мы не допускали мысли, что все великолепные передатчики могут разом выйти из строя. Однако нам никто не отвечал, и, понимая, какой бешеной силы мог там достигать шквалистый ветер, мы строили самые ужасные догадки.");
                        }
                        if (time.getTime().equals("5 января в 17:15")) {
                            System.out.println("Ведь радио по-прежнему молчало, никто не отвечал на наши постоянные вызовы.");
                        }
                    }
                    else {
                        System.out.println("Мы связались по радио с: " + Name);
                        if (Name.equals("Шерман")) {
                            System.out.println("Я приказал ему срочно вылететь ко мне, взяв матросов.");
                        }
                        if (Name.equals("Дуглас")) {
                            System.out.println("Дуглас сказал мне, что также не смог вызвать Лейка на связь. Об урагане он узнал от меня -- в районе залива Мак-Мердо было тихо, хотя в это верилось с трудом.");
                            time.setTime("Весь день");
                        }
                    }
                } catch (NameException e) {
                    System.out.println(e.toString());
                }
            }
        };
        void useRadio(String name, Time time) {
            radio.connect(name, time);
        }
    }

    void usePlane(Plane plane, EPlace toPlace, Time time) {
        if (plane.getPlace() == EPlace.McMerdo) {
            System.out.println(plane.getDeviceName() + " в " + plane.getPlace() + " находился в полной готовности, оснащенный для таких вот крайних ситуаций. По всему было видно, что " + time.getTime());
        }
        if (plane.getPlace() == EPlace.Base) {
            System.out.println(plane.getDeviceName() + ", позволял это сделать.");
        }
        else if (plane.getPlace() == EPlace.Flight) {
            if (time.getTime().equals("19:30")) {
                System.out.println("Шерман с матросами взлетели в " + time.getTime());
                plane.move(toPlace);
                System.out.println("За время полета они несколько раз информировали нас о том, что все идет хорошо.");
                time.setTime("00:00.");
                System.out.println("Они достигли базы в " + time.getTime());
            }
            if (time.getTime().equals("5 января в 17:15")) {
                System.out.println(time.getTime() + " начался наш полет на север в самолете, который вел пилот Мактай. Кроме него в самолете находились еще десять человек, семь собак, сани, горючее, запас продовольствия, а также прочие необходимые вещи, в том числе и рация.");
            }
        }
    }

    public boolean haveName() throws ExistException{
        if (Name==null)throw new ExistException("Поле Имя");
        return Name.equals("") ? false : true;
    }
}
