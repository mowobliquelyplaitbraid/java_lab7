import java.io.Serializable;
import java.time.LocalDateTime;

public class Plane extends Technics implements IMove, Serializable, Comparable<Plane> {
    private final String name;
    private Human human;
    private EPlace place;
    private int number;
    private LocalDateTime date;
    private String login;

    public int compareTo(Plane other) {
        return this.getDeviceName().compareTo(other.getDeviceName());
    }

    Plane(String n, Human h, EPlace p, int num) {
        place = p;
        name = n;
        human = h;
        number = num;
        date = LocalDateTime.now();
    }

    Plane(String n, Human h, EPlace p, int num, LocalDateTime d, String l) {
        place = p;
        name = n;
        human = h;
        number = num;
        date = d;
        login = l;
    }

    public void move(EPlace p) {
        place = p;
    }

    public EPlace getPlace() {
        return place;
    }

    public String getDeviceName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public String getDate() {
        return date.toString();
    }

    public String getOwner() {
        return human.getName();
    }

    public EPlace getOwnerPlace() {
        return human.getPlace();
    }

    void setDate() {
        date = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Plane named " + this.name + " belonging to " + human.getName() + " located at " + this.place +
                " with serial number " + this.number + " created at " + this.date;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Plane p = (Plane) obj;
        return name.equals(p.name) && place == p.getPlace() && getOwner().equals(p.getOwner());
    }
}