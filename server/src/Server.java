import com.google.gson.Gson;

import java.io.*;
import java.net.*;
import java.util.Vector;
import java.sql.*;

public class Server extends Thread {
    private DatagramSocket udpSocket;
    private int port;
    private String fileName = "InitData.json";
    Vector<Plane> planes = new Vector<>();

    /**
     * Constructor for server creation
     *
     * @param port  - Port
     */
    public Server(int port) throws IOException {
        this.port = port;
        this.udpSocket = new DatagramSocket(port);
        System.out.println("Connection to the server was successful.");
        System.out.println("UDP address: " + InetAddress.getLocalHost());
        System.out.println("UDP port: " + this.port);
//        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader((new FileInputStream(fileName))));
//            Plane[] p = new Gson().fromJson(reader, Plane[].class);
//            for (Plane element : p) {
//                element.setDate();
//                planes.add(element);
//            }
//        } catch (FileNotFoundException e) {
//            System.out.println("This file doesn't exist yet or you don't have permission for reading. Please, create one and try again.");
//        } catch (Exception e) {
//            System.out.println("Your input file is empty or has some wrong data in it.");
//        }
    }

    /**
     * Method for processing the command and sending the result to the client
     *
     */
    @Override
    public void run() {
        while (true) {
            byte[] buffer = new byte[8190];
            DatagramPacket response = new DatagramPacket(buffer, buffer.length);
            try {
                udpSocket.receive(response);
            } catch (IOException e) {
                System.out.println("Problems with receiving.");
            }
            try (ByteArrayInputStream inStream = new ByteArrayInputStream(buffer);
                 ObjectInputStream input = new ObjectInputStream(inStream);
                 ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                 ObjectOutputStream output = new ObjectOutputStream(outStream)) {
                Command com = (Command) input.readObject();
                if (com.getCommand() != "import") {
                    com.setPlanes(planes);
                }
                com.swCommand();

                com.sortPlanes();
                output.flush();
                output.writeObject(com);
                buffer = outStream.toByteArray();
                DatagramPacket bResponse = new DatagramPacket(buffer, buffer.length, response.getAddress(), response.getPort());
                udpSocket.send(bResponse);
                planes = com.getPlanes();
            } catch (IOException e) {
                System.out.println("Problems with streams, huh.");
            } catch (ClassNotFoundException e) {
                System.out.println("Oooooh, serialization.");
            }
        }
    }

    public static void main(String[] args) {
        try {
            String driverName = "org.postgreesql.Driver";
            Class.forName(driverName);
//            Server server = new Server(Integer.parseInt(args[0]));
            Server server = new Server(6666);
            server.start();
        } catch (IOException e) {
            System.out.println("Problems with port or host.");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
