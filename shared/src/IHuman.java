public interface IHuman {
    String getName();
    void getScared(Time time);
    void talkToOthers(Time time);
    void reflection(Time time);
}
